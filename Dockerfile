FROM index.alauda.cn/alaudaorg/alpine-with-nginx-vts:latest

# RUN apk add --no-cache make

# COPY ./requirements.txt /jakiro/
WORKDIR /test
EXPOSE 80
CMD ["/test/run.sh"]

COPY . /test

